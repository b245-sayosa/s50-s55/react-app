import AppNavbar from '../src/components/AppNavBar.js';
import './App.css';
import Home from './pages/Home.js'
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
// import Highlights from './Highlights.js';
// import Banner from './Banner.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js'
import Login from './pages/Login.js';
//import react router dom
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Logout from './pages/Logout.js';
import Pagenotfound from './pages/Pagenotfound.js';


function App() {
  return (
    <Router>
    <AppNavbar/>
    <Routes>
       <Route path="/" element = {<Home/>}/>
       <Route path="/courses" element = {  <Courses/> }/>
       <Route path="/login" element = {  <Login/> }/>
       <Route path="/register" element = {  <Register/> }/>
       <Route path="/logout" element = {  <Logout/> }/>
       <Route path ="*" element = {<Pagenotfound/>}/>
    </Routes>
    </Router>
  );
}

export default App;
