import coursesData from "../db/courses.js"
import Coursecard from "../components/Coursecard.js"
import { Fragment } from "react"


export default function Courses(){
    console.log(coursesData)

    const courses = coursesData.map(course =>{
        return( <Coursecard key =  {course.id} courseProp = {course}/>
        
        )
    })
    
    return(
        <Fragment>
        {courses}
        </Fragment>
    )
}