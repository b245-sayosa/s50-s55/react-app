import { Fragment, useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Navigate } from "react-router-dom";


export default function(){
    //Create 3 new states where we will store the value from input of the email, password and confirmPassword
    const [email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const[confirmPassword, setConfirmPassword] = useState('');

    const [isActive, setIsActive] = useState(false);
    const [user, setUser] = useState(localStorage.getItem("email"))
    
    useEffect(()=>{
      if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
          setIsActive(true);
      }else{
        setIsActive(false);
      }
  
    }, [email, password, confirmPassword])
  
    function register(event){
      event.preventDefault();
  
      alert("Congratulations, you are now registered on our website!");
  
      setEmail('');
      setPassword('');
      setConfirmPassword('');
  
    }
    
   

    return (
      user ?
      <Navigate to = "/"/>
      :
      <Fragment>
        <h1 className="text-center mt-5">Register</h1>

        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address:</Form.Label>
            <Form.Control 
            type="email" 
            placeholder="Enter email"
            value={email}
            onChange = {event => setEmail(event.target.value)}
             />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
    
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password: </Form.Label>
            <Form.Control 
            type="password" 
            placeholder="Password"
            value = {password}
            onChange = {event => setPassword(event.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formChangePassword">
            <Form.Label>Confirm Password:</Form.Label>
            <Form.Control 
            type="password" 
            placeholder="Confirm Password"
            value = {confirmPassword}
            onChange = {event => setConfirmPassword(event.target.value)}
            
            />
          </Form.Group>
         {/*In this code block we do conditional rendering depending on the state of our isActive*/}
		     {
		     	isActive ?
		     	<Button variant="primary" type="submit">
		     	  Submit
		     	</Button>
		     	:
		     	<Button variant="danger" type="submit" disabled>
		     	  Submit
		     	</Button>
		     }
        </Form>
 </Fragment>
      );
}