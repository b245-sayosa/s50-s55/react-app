import React, { Fragment } from "react";
import { Nav } from "react-bootstrap";
import { NavLink} from "react-router-dom";


export default function(){
    return(
     
        <Fragment>
            <h1>Error404</h1>
            <h1>Page Not Found</h1>
            <h1>Go Back to</h1>
			<Nav.Link as = {NavLink} to = "/">Homepage</Nav.Link>
			</Fragment>
            
       
    )
}