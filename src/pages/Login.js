import { Fragment, useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { Navigate} from 'react-router-dom'



export default function(){
    //Create 3 new states where we will store the value from input of the email, password and confirmPassword
    const [email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    // const[confirmPassword, setConfirmPassword] = useState();
    const [user, setUser] = useState(localStorage.getItem('email'))
    
    useEffect(()=>{
        console.log(email)
        console.log(password)
        // console.log(confirmPassword)
    }, [email, password])  

    function login(event){
        event.preventDefault()

        localStorage.setItem("email", email);
        alert("Check")
        setEmail('')
        setPassword('')
    }


    return (
        user ?
        <Navigate to = "/"/>
        :   
        <Fragment>
        <h1 className="text-center mt-5">Login</h1>

        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address:</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
    
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password: </Form.Label>
            <Form.Control 
            type="password" placeholder="Password"
            value = {password}
            onChange = {event => setPassword(event.target.value)}
            />
          </Form.Group>

      
          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
 </Fragment>
      );
}