//import BOOTSTRAP classes
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import { Fragment, useState } from 'react';


export default function AppNavbar(){
	console.log(localStorage.getItem("email"))

	//new state for the user
	const [user, setUser] = useState(localStorage.getItem("email"))


	return(
		<Navbar bg="light" expand="lg">
		      <Container>
		        <Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>
					{/* cond render */}
					{
						user ?
						<Nav.Link as = {NavLink}>Logout</Nav.Link>
						:
						<Fragment>
						<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
		            	<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
						</Fragment>
					}



{/* 
					<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
		            <Nav.Link as = {NavLink} to = "/login">Login</Nav.Link> */}
		          
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)

}