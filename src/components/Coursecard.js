import { useState, useEffect } from 'react';
import{ Row, Col, Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';



export default function Coursecard({courseProp}) {
        // console.log(course.id)

        const { name, description, price} = courseProp


        // use the state hook for this component to beable to use its state
        // //states are used to keep track of information
        // cons [getElementError, setter] = useState(initalGetterValue)

        const [enrollees, setEnrollees] = useState(0);

        const [availableSeat, setAvailable] = useState(30);

        const [isDisabled, setIsDisabled] = useState(false);
        
        // initial values of enrollees
        // setEnrollees(1)
        // console.log(enrollees)
  function enroll(){
  
    if(availableSeat > 1 && enrollees < 29 ){
      setEnrollees(enrollees+1)
      setAvailable(availableSeat-1)

      
    }else{
        return alert("Congratulations! for making it")
        setEnrollees(enrollees+1)
        setAvailable(availableSeat-1)
       
      }  
  }

  //define a useeffect hook to have
  // syntax

  //useEffect(side effect/ , [dependencies])
      
      useEffect(() => {
        if(availableSeat === 0){
          setIsDisabled(true)
        }

        
      }, [availableSeat])
      

  
  




        

  
  return (
    <Row className='pt-5'>
        <Col>
           <Card>
              <Card.Header>{name}</Card.Header>
              <Card.Body>
                <Card.Title>Description:</Card.Title>
                <Card.Text>
                  {description}
                </Card.Text>
                <Card.Title>Price:</Card.Title>
                <Card.Text>
                PHP{price}
                </Card.Text>
                <Card.Title>Enrollees:</Card.Title>
                <Card.Text>
                {enrollees}
                </Card.Text>
                <Card.Title>Available Seats:</Card.Title>
                <Card.Text>
                {availableSeat}
                </Card.Text>
                
                <Button variant="primary" onClick={enroll} disabled = {isDisabled}>Enroll Now!</Button>
              </Card.Body>
            </Card>
        </Col>
    </Row>  
          );
}